package com.example.helloworld.entity;
import java.io.Serializable;
import java.util.Date;

import java.util.List;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.MappedSuperclass;


@MappedSuperclass
@DynamicUpdate

public class BaseEntity implements Serializable {
}
