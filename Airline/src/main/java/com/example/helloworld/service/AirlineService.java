package com.example.helloworld.service;

import com.example.helloworld.entity.Airline;
import com.example.helloworld.entity.Student;
import com.example.helloworld.responsitory.AirlineReponsitory;
import com.example.helloworld.responsitory.StudentReponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AirlineService {
    @Autowired
    private AirlineReponsitory repo;

    public List<Airline> allAirline(){
        List<Airline> airlines = repo.findAll();
        return airlines;
    }

}
