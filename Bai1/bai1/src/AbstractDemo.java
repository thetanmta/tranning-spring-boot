public class AbstractDemo {
    public static void main(String [] args) {
        /* Following is not allowed and would raise error */
        Employee e = new Employee("George W.", "Houston, TX", 43) {
            @Override
            public String getName() {
                return super.getName();
            }
        };
        System.out.println("\n goi cho email");
        e.mailCheck();
    }
}
