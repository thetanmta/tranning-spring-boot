package com.example.helloworld.responsitory;
import java.io.Serializable;

import com.example.helloworld.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean

public interface BaseReponsitory<T extends BaseEntity,ID extends Serializable>
        extends JpaRepository<T , ID>  {


}
