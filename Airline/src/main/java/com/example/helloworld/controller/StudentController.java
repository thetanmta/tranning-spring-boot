package com.example.helloworld.controller;

import com.example.helloworld.entity.Airline;
import com.example.helloworld.entity.Student;
import com.example.helloworld.service.AirlineService;
import com.example.helloworld.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;



import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.transaction.Transactional;

@Controller
@Transactional

public class StudentController {
    @Autowired
    private StudentService studentService;
    @Autowired
    private AirlineService airlineService;

    //hien thi trang home
    @RequestMapping(value={"/","/home"})
    public String home(Model model) {
        List<Student> allStudents = studentService.allStudent();
        model.addAttribute("allStudents", allStudents);
        return "home";
    }

    @RequestMapping(value={"/","/airline"})
    public String airline(Model model) {
        List<Airline> allAirlines = airlineService.allAirline();
        model.addAttribute("allAirlines", allAirlines);
        return "airline";
    }


}
