package com.example.helloworld.responsitory;
import java.util.List;

import com.example.helloworld.entity.Airline;
import com.example.helloworld.entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AirlineReponsitory extends BaseReponsitory<Airline,Integer>{
    @Query(value = "SELECT * FROM Data", nativeQuery = true)
    List<Airline> findAll();
}
