package com.example.helloworld.responsitory;

import java.util.List;

import com.example.helloworld.entity.BaseEntity;
import com.example.helloworld.entity.Student;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;


@Repository

public interface StudentReponsitory extends BaseReponsitory<Student,Integer> {
    @Query(value = "SELECT * FROM student", nativeQuery = true)
    List<Student> findAll();
}
