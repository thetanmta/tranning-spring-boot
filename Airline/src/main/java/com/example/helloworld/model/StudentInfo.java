package com.example.helloworld.model;

import com.example.helloworld.entity.Student;

import java.util.Date;

public class StudentInfo {
    private  String id;
    private String ten;
    private Date ngaySinh;
    private String diaChi;

    public StudentInfo(){

    }
    public  StudentInfo(Student student){
        this.id=student.getId();
        this.ten=student.getTen();
        this.ngaySinh=student.getNgaySinh();
        this.diaChi=student.getDiaChi();

    }
    public StudentInfo(String id,String ten,Date ngaySinh,String diaChi){
        this.id=id;
        this.ten=ten;
        this.ngaySinh=ngaySinh;
        this.diaChi=diaChi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
}
