package com.example.helloworld.service;

import com.example.helloworld.entity.Student;
import com.example.helloworld.responsitory.StudentReponsitory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Pageable;


import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class StudentService {
    @Autowired
    private StudentReponsitory repo;

    public List<Student> allStudent(){
        List<Student> students = repo.findAll();
        return students;
    }

}
