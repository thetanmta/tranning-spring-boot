package com.example.helloworld.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Data
@Getter
@Setter
@Table(name = "Data")
public class Airline extends BaseEntity implements Serializable {
    @Id
    @Column(name = "airport_code", length = 200, nullable = false)
    private String airport_code;
    @Column(name = "airport_name", length = 200, nullable = false)
    private  String airport_name;
    @Column(name = "statistics_flights_cancelled", length = 200, nullable = false)
    private int flights_cancelled;
    @Column(name = "statistics_flights_ontime", length = 200, nullable = false)
    private int statistics_flights_ontime;
    @Column(name = "statistics_flights_total", length = 200, nullable = false)
    private int statistics_flights_total;
    @Column(name = "statistics_flights_delayed", length = 200, nullable = false)
    private int statistics_flights_delayed;
    @Column(name = "statistics_flights_diverted", length = 200, nullable = false)
    private int statistics_flights_diverted;
    @Column(name = "statistics_of_delays_late_aircraft", length = 200, nullable = false)
    private int statistics_of_delays_late_aircraft;
    @Column(name = "statistics_of_delays_weather", length = 200, nullable = false)
    private int statistics_of_delays_weather;
    @Column(name = "statistics_delays_security", length = 200, nullable = false)
    private int statistics_delays_security;
    @Column(name = "statistics_delays_national_aviation_system", length = 200, nullable = false)
    private int statistics_delays_national_aviation_system;
    @Column(name = "statistics_delays_carrier", length = 200, nullable = false)
    private int statistics_delays_carrier;
    @Column(name = "statistics_minutes_delayed_late_aircraft", length = 200, nullable = false)
    private int statistics_minutes_delayed_late_aircraft;
    @Column(name = "statistics_minutes_delayed_weather", length = 200, nullable = false)
    private int statistics_minutes_delayed_weather;
    @Column(name = "statistics_minutes_delayed_carrier", length = 200, nullable = false)
    private int statistics_minutes_delayed_carrier;
    @Column(name = "statistics_minutes_delayed_security", length = 200, nullable = false)
    private int statistics_minutes_delayed_security;
    @Column(name = "statistics_minutes_delayed_total", length = 200, nullable = false)

    private int statistics_minutes_delayed_total;
    @Column(name = "statistics_minutes_delayed_national_aviation_system", length = 200, nullable = false)
    private int statistics_minutes_delayed_national_aviation_system;
    @Column(name = "time_label", length = 200, nullable = false)
    private  String time_label;
    @Column(name = "time_year", length = 200, nullable = false)
    private int time_year;
    @Column(name = "time_month", length = 200, nullable = false)
    private int time_month;
    @Column(name = "carrier_code", length = 200, nullable = false)
    private  String carrier_code;
    @Column(name = "carrier_name", length = 200, nullable = false)
    private  String carrier_name;















}
