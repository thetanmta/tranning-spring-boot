package com.example.helloworld.entity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "student")

public class Student extends BaseEntity implements Serializable {
    @Id
    @Column(name = "id", length = 200, nullable = false)
    private  String id;
    @Column(name = "Ten", length = 200, nullable = false)
    private String ten;
    @Column(name = "ngay_sinh", length = 200, nullable = false)
    private Date ngaySinh;
    @Column(name = "dia_chi", length = 200, nullable = false)
    private String diaChi;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
}
